package dev.test.onlinephotoviewer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import uk.co.senab.photoview.PhotoView;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActionBar().hide();
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        @InjectView(R.id.view_pager)
        ViewPager mPager;

        private static ArrayList<String> DEMO_PHOTO_LIST;
        static{
            DEMO_PHOTO_LIST = new ArrayList<String>();

            DEMO_PHOTO_LIST.add("http://www.destination360.com/contents/pictures/canada/great-lakes.jpg");
            DEMO_PHOTO_LIST.add("http://www.lakescientist.com/wp-content/uploads/2011/07/Lake-Michigans-Sleeping-Bear-Dunes.jpg");
            DEMO_PHOTO_LIST.add("http://www.earthrangers.com/content/wildwire/lake_view.jpg");
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            //Inject all views under rootView
            ButterKnife.inject(this, rootView);

            //Control your view pager
            mPager.setAdapter(new PhotoPagerAdapter(getActivity(), DEMO_PHOTO_LIST));

            return rootView;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();

            //Reset and clear all views
            ButterKnife.reset(this);
        }

        private static class PhotoPagerAdapter extends PagerAdapter{

            Context mContext;
            List<String> mPhotoList;

            private PhotoPagerAdapter(Context mContext, List<String> mPhotoList) {
                this.mContext = mContext;
                this.mPhotoList = mPhotoList;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {

                //An ImageView supports zoom in, zoom out, pan
                PhotoView photoView = new PhotoView(mContext);

                //Load photo on Internet
                Picasso.with(mContext)
                        .load(mPhotoList.get(position))
                        .into(photoView);

                //Add as child view
                container.addView(photoView);

                return photoView;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }

            @Override
            public int getCount() {
                return mPhotoList.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }
        }
    }
}
